# reign-test

## How to start?

Just run  
`docker-compose up --build`


No need to setting up any DB or framework (docker magic :) )

## Reminders

1. I left the Cron to a 1 minute period, just for you to test the app easier
2. There is no need of reloading the page, everything is synchronized