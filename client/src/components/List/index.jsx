import React from "react";
import axios from "axios";
import List from "@material-ui/core/List";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import ListItem from "@material-ui/core/ListItem";
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import "./style.css";

class ListAlter extends React.Component {
  state = {
    news: [],
  };

  componentDidMount() {
    axios.get("/api/news").then((response) => {
      this.setState({ news: response.data.data });
    });
  }

  onClickHandler(element) {
    axios.put(`/api/news/${element._id}`).then(() => {
      let newArr = this.state.news
      let index = newArr.indexOf(element);
      newArr.splice(index,1);
      this.setState({ news: newArr });
    });

  }

  render() {
    const renderName = (element) => {
      if (element.story_title) {
        return <span className="newsTitle">{element.story_title}</span>;
      } else {
        return <span className="newsTitle">{element.title}</span>;
      }
    };

    const renderDate = (date) => {
       var parsed = new Date(date);
       var dateNow = new Date(Date.now());
       var currentParsed = dateNow.toLocaleDateString().split('/')
       var dateParsed = parsed.toLocaleDateString().split('/')

       var today = currentParsed[2] === dateParsed[2] && currentParsed[0] === dateParsed[0] && currentParsed[1] === dateParsed[1]
       var yesterday = currentParsed[2] === dateParsed[2] && currentParsed[0] === dateParsed[0] && parseInt(currentParsed[1])-1 === parseInt(dateParsed[1])

       if( today )
        return parsed.toLocaleTimeString()
       else{
           if( yesterday){
            return 'Yesterday'
           }
           else{
            var n =  parsed.toUTCString().split(" ")
            return n[2]+" "+n[1]
           }
       }
    }

    return (
      <List>
        {this.state.news &&
        // eslint-disable-next-line
          this.state.news.map((element) => {
            if (element.story_title || element.title) {
              return (
                <>
                  <ListItem
                    button
                    component="a"
                    href={element.story_url ? element.story_url : element.url}
                  >
                    <Container style={{ paddingTop: "12px",paddingBottom: "12px", paddingRight: "40px",}}>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <div>
                          {renderName(element)}
                          <span className="newsSubtitle">
                            {" "}
                            - {element.author} -{" "}
                          </span>
                        </div>
                        <span>{renderDate(element.created)}</span>
                      </div>
                    </Container>
                    <ListItemSecondaryAction>
                      <IconButton
                        onClick={() => this.onClickHandler(element)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                </>
              );
            }
          })}
      </List>
    );
  }
}

export default ListAlter;
