import React from 'react';
import './App.css';
import ListAlter from './components/List/index'
import Container from '@material-ui/core/Container';
import FavoriteIcon from '@material-ui/icons/Favorite';

function App() {
  return (
    <div className="App">
      <div className="hero">
        <div className="titles">
          <span className="title">
            HN Feed
          </span>
          <p className="subtitle">
            We <FavoriteIcon style={{marginTop: "20px", color:"red"}}/> hacker news!
          </p>
        </div>
      </div>
      <Container>
        <ListAlter />
      </Container>
    </div>
  );
}

export default App;
