News = require('../models/newsModel')

exports.index = (req, res) => {
    News.find({active: true}).sort({created: -1}).exec((err, news) => {
        if (err)
            res.json({
                status: "error",
                message: err
            });
        res.json({
            data: news
        });
    })
}

exports.add = (req, res) => {
    var news = new News();
    console.log(req.body)
    news.created = req.body.created_at;
    news.title = req.body.title;
    news.url = req.body.url;
    news.author = req.body.author;
    news.story_title = req.body.story_title;
    news.story_url = req.body.story_url;
    news.object_id = req.body.objectID;
    news.save((err) => {
        if (err)
            res.json(err); res.json({
                message: "New News Added!",
                data: news
            });
    });
};

exports.update = (req, res) => {
    News.findById(req.params.news_id, (err, news) => {
        if (err)
            res.send(err);
        news.active = false
        news.save((err) => {
            if (err)
                res.json(err)
            res.json({
                message: "Message Updated Successfully",
                data: news
            });
        });
    });
};