const axios = require('axios')
News = require('../models/newsModel')

const save = (data) => {
    News.findOne({ object_id: data.objectID }, (err, result) => {
        if (err) {
            console.log(err)
        }
        if (result) {
            console.log("already saved");
        }
        else {
            var news = new News();
            news.created = data.created_at;
            news.title = data.title;
            news.url = data.url;
            news.author = data.author;
            news.story_title = data.story_title;
            news.story_url = data.story_url;
            news.object_id = data.objectID;
            news.save((err) => {
                if (err)
                    console.log(err)
                else
                    console.log("New element added to News")
            });
        }
    })
}

const reloadDatabase = () => {
    axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .then(response => {
            var data = response.data.hits;
            for (var index = 0; index < data.length; index++) {
                save(data[index])
            }
        })
        .catch(error => {
            console.log(error);
        });
}

exports.reloadDatabase = reloadDatabase;