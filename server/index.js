const bodyParser = require('body-parser');
const express = require('express')
const mongoose = require('mongoose')
const cron = require('node-cron')
const mainController = require('./controllers/mainController')

const app = express();

var port = process.env.PORT || 5000;

app.get('/', (req, res) => res.send('Welcome to '));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const apiRoutes = require('./routes')
app.use('/api', apiRoutes)

mainController.reloadDatabase();

cron.schedule('* * * * *', () => {
  mainController.reloadDatabase();
});

app.listen(port, () => {
  console.log("Running FirstRest on Port " + port);
})

mongoose.connect('mongodb://database/mydatabase')
    .then( db => console.log('Db is connected to ',db.connection.host))
    .catch( err => console.log(err));

