const router = require('express').Router();

router.get('/', function(req, res) {
    res.json({
        status: 'API Works',
        message: 'Welcome to FirstRest API'
    });
});

const newsController = require('./controllers/newsController');

router.route('/news')
    .get(newsController.index)
    .post(newsController.add);
    
router.route('/news/:news_id')
    .patch(newsController.update)
    .put(newsController.update)

module.exports = router;