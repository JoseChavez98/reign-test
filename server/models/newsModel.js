var mongoose = require('mongoose')

var newsSchema = mongoose.Schema({
    created: Date,
    title: String,
    url: String,
    author: String,
    story_title: String,
    story_url: String,
    object_id: String,
    active: { type: Boolean, default: true}
})

var News = module.exports = mongoose.model('news', newsSchema)

module.exports.get = (callback, limit) => {
    News.find(callback).limit(limit)
}